import { h } from 'preact';
import { Link } from 'preact-router/match';
import style from './style';

const Header = () => (
	<header class={style.header}>
		<h1>Hacker News</h1>
		<nav>
			<Link activeClassName={style.active} href="/">Top</Link>
			<Link activeClassName={style.active} href="/new">New</Link>
			<Link activeClassName={style.active} href="/show">Show</Link>
			<Link activeClassName={style.active} href="/ask">Ask</Link>
			<Link activeClassName={style.active} href="/jobs">Jobs</Link>
			<Link activeClassName={style.active} href="/profile">Me</Link>
			{/* <Link activeClassName={style.active} href="/profile/john">John</Link> */}
		</nav>
	</header>
);

export default Header;
