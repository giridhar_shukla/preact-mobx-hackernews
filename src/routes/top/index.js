import { h } from 'preact';
import style from './style';

const Top = () => (
	<div class={style.home}>
		<h1>Top</h1>
		<p>Top news is right here.</p>
	</div>
);

export default Top;
